# featext

<!-- badges: start -->
<!-- badges: end -->

The goal of featext is to provide a package for the computation of statistical, spectral and temporal features over time series data.

## Installation


To install through git, use

``` r
# install.packages("devtools")
devtools::install_git("https://gitlab.com/krpack/featext.git")
```


To install from a local folder, use

``` r
# install.packages("devtools")
devtools::install(path_to_package)
```

## Usage

This is a basic example which shows you how to call the main function of the package and compute a set of features over a dataset:

``` r
library(featext)
feature_dataset <- compute_features(dataset,
                                    # names of the columns to use
                                    sens_cols = sens_cols,
                                    # compute all exisisting features
                                    feature_list =  get_all_computable_features(),
                                    # windows specification
                                    window_length=60, window_overlap=30,
                                    sampling_frequency = 50,
                                    # additional arguments for autocorrelation and percentile features
                                    lag = 1, percentile = .25)
```

For more examples and full documentation visit https://krpack.gitlab.io/featext/index.html.

