context("check calculation of statistical features")

test_that("stat_mode works", {

  #handle NAs
  expect_equal(stat_mode(c(rep(9, 3), 5, 2, NA_real_)), 9)
  #when na.rm=F and Mode=NA returns NA
  expect_equal(stat_mode(c(rep(9, 3), 5, 2, rep(NA_real_, 7))), NA_real_)
  #na.rm works
  expect_equal(stat_mode(c(rep(9, 3), 5, 2, rep(NA_real_, 7)), na.rm = T), 9)
  #when values frequencies are all equal returns NA
  expect_equal(stat_mode(c(9, 3, 5, 7)), NA_real_)
  #when the vector is plurimodal and plurimodal=F returns NA
  expect_equal(stat_mode(c(rep(9,2), rep(3,2), 5, 7)), NA_real_)
  #when the vector is plurimodal and plurimodal=T returns a vector of multiple ascendent Modes
  expect_equal(stat_mode(c(rep(9,2), rep(3,2), 5, 7), plurimodal = T), c(3, 9))
  #when a vector consists of all NAs and na.rm=F return NA
  expect_equal(stat_mode(c(rep(NA_real_, 3))), NA_real_)
  #when a vector consists of all NAs and na.rm=T returns NA
  expect_equal(stat_mode(c(rep(NA_real_, 3)), na.rm = T), NA_real_)



  # check that error is correctly catched by check input
  expect_error(stat_mode(list(c(4, 7, 9), c(3, 1, 8))), "input is not valid")

})

