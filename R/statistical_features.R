

#' Mode calculation
#'
#' @param x A numeric vector or an object previously checked with
#'   \code{\link{check_input}}.
#' @param na.rm Logical, should NAs be removed?
#' @param plurimodal Logical, when x is multimodal if plurimodal is TRUE all
#'   modes will be returned, otherwise NA_real_.
#' @param checkinput Logical, by default set to TRUE. It checks the input format.
#'
#' @return A numeric element or, if plurimodal is TRUE and x is multimodal, a
#'   numeric vector.
#' @export
#'
#' @family statistical features

stat_mode <- function(x, na.rm = F, plurimodal = F, checkinput = T) {
  if(checkinput){
    if( !check_input(x) ){
      stop(paste0("Can't compute statistical features, the input is not valid."))
    }
  }
  ux <- unique(x)
  if (na.rm == T) {
    indices <- which(is.na(ux))
    if (length(indices) != 0) {
      ux <- ux[-indices]
    }
  }
  if (length(ux) == 0) { #si verifica se x aveva tutti NA e na.rm=TRUE
    return(NA_real_)
  }
  counts <- tabulate(match(x, ux))
  Mode <- ux[which(counts == max(counts))]
  Mode <- Mode[order(Mode)]
  if (length(Mode) != 1 && plurimodal == F) { #L'uguaglianza si ha quando x ha elementi tutti distinti
    Mode <- NA_real_
    return(Mode)
  } else {
    return(Mode)
  }

}

