% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/temporal_features.R
\name{temporal_zcr}
\alias{temporal_zcr}
\title{Zero crossing rate of a time series}
\usage{
temporal_zcr(x, sampling_freq, checkinput = T)
}
\arguments{
\item{x}{A numeric vector or an object previously checked with
\code{\link{check_input}}.}

\item{sampling_freq}{A numeric element representing the signal sampling
frequency (in Hz).}

\item{checkinput}{Logical, by default set to TRUE. It checks the input format.}
}
\value{
A numeric element.
}
\description{
Zero crossing rate of a time series
}
\seealso{
\code{\link[seewave::zcr]{seewave::zcr}}

Other temporal features: \code{\link{autocorrelation}},
  \code{\link{compute_features}},
  \code{\link{temporal_centroid}}
}
\concept{temporal features}
