% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utilities.R
\name{compute_power_spectrum}
\alias{compute_power_spectrum}
\title{Power spectrum calculation}
\usage{
compute_power_spectrum(x, sampling_freq)
}
\arguments{
\item{x}{A numeric vector or an object previously checked with
\code{\link{check_input}}.}

\item{sampling_freq}{A numeric element representing the signal sampling
frequency (in Hz).}
}
\value{
A two-column matrix where the first column corresponds to the
  frequency axis (in Hz) and the second column corresponds to the amplitude
  axis of the power spectrum of a signal.
}
\description{
Compute the power density spectrum. Linear trend is removed from the time series prior to the computation of the spectrum.
}
\seealso{
\code{\link[seewave::spec]{seewave::spec}}
}
